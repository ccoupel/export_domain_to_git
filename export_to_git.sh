#!/bin/bash                                  
git_server="bitbucket.org"              
git_project="miq_project"                 
git_branch="dev"         
git_user="ccoupel"                                
git_mail="ccoupel@redhat.com"
domain="MAIN_DOMAIN"

tmp_dir="/tmp/export_to_git"

local_dir=$(pwd)

miqexport="/var/www/miq/vmdb/cfme-rhconsulting-scripts/bin/miqexport"
############# checking 
if [[ ! -f "$miqexport" ]] 
then
	echo "'$miqexport' must be installed; please check https://github.com/rhtconsulting/cfme-rhconsulting-scripts"
       exit 10
fi

read -sp "give GIT password for user [$git_user]: " git_password
echo
mkdir "$tmp_dir" 2> /dev/null
cd "$tmp_dir"

git config --global http.sslVerify false
git config --global user.email "$git_mail"
git config --global user.name "$git_user"

#remove old GIt info
rm -rf git                                                                                                                                                                             [23/64]
#remove old domains
rm -rf exports
mkdir exports

#export domains
echo "####### EXPORTING from CFME #######"
"$miqexport" all ./exports 
#/var/www/miq/vmdb/cfme-rhconsulting-scripts/bin/miqexport domain "$domain" ./exports 

#refresh git repo
echo "###### synch git repo #####"
mkdir git
cd git
git clone https://$git_user:$git_password@$git_server/cfme/$git_project
cd "$git_project"
git checkout "$git_branch"


#remove actual domains in repo
echo "##### pushing domain to git repo #####"
rm -rf "$domain"

#move ESKM domain to repo
cp -rp ../../exports/automate/"$domain" .
cp -rp ../../exports/service_dialogs .
#cp -rp ../../exports/"$domain" .

# put the current script to git
cp  "$local_dir/$0" .

echo "##### commit changes #####"
#add and commit
git add -A .
git status
git commit -m "$1"
read -p "press [CTRL]+C if you dont want to push your changes"
echo "##### push changesto git #####"
git push

echo "##############################"
echo "###### [$1] pushed to git ######"
echo "##############################"

