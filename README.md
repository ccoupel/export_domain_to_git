The transfer of the automation from the Dev environment to the Prod environment is made via the GIT server. The automation is pushed to GIT to the DEV branch on every update of the automation.

The changes are then merged from the Dev to the Prod via GIT change requests

The MAIN automation domain of the PROD environment is liked to the Git Master branch , so a refresh of the datastore domain is then needed to retrieve the updates.

The export script is based on the rhconsulting scripts available in https://github.com/rhtconsulting/cfme-rhconsulting-scripts

The configuration parameters are located inside the script :

- git_server="bitbucket.org" => server name/ip of the git server
- git_project="miq_main" => project name where the automation is gitted
- git_branch="dev" => branch where to push the the automation to
- git_user="cfme" => user to log in to the GIT server
- git_mail="ccoupel@redhat.com" => mail of the git user
- domain="MAIN" => automation domain to push to git

to export to the git branch, use the script as follow :
[root@miq_server ~]# ./export_to_git.sh <"desciption">
